// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';
//import client from 'twilio';
const client = require('twilio')(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { result } = context;
    client.messages.create({
            from: process.env.TWILIO_PHONE_NUMBER,
            to: result.to,
            body: result.msg
        })
        .then(() => {
            console.log("SMS sent to: " + result.to);
        })
        .catch((err:any) => {
          console.log(err);
        });
    return context;
  };
}