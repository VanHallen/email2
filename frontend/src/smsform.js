import React, { Component } from 'react';
import client from './feathers';

class Smsform extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  sendSms(ev) {
    ev.preventDefault();
    const inputTo = ev.target.querySelector('[id="to"]');
    const inputMsg = ev.target.querySelector('[id="msg"]');
    const to = inputTo.value.trim();
    const msg = inputMsg.value.trim();

    console.log( "To: " + to );

    this.state.sms.create({
      to,
      msg
    })
    .then(() => {
      inputTo.value = '';
    });
  }

  componentDidMount() {
    const sms = client.service('sms');

    this.setState({sms});
  }
  
  render() {
    return(
    <div>
      <div className="py-5 text-center">
        <h2>Send SMS</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <h4 className="mb-3">SMS</h4>
          <form onSubmit={this.sendSms.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-12 mb-3">
                <label htmlFor="desc">To</label>
                <input type="tel" className="form-control" id="to" defaultValue="" required />
                <input type="text" className="form-control" id="msg" defaultValue="" required />
                <div className="invalid-feedback">
                    A description is required.
                </div>
              </div>
            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Send SMS</button>
          </form>
        </div>
      </div>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Smsform;